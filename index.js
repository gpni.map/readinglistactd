console.log("Hello World");



//Part 1:
/*
Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.
1.)
Create a student class sectioning system based on their entrance exam score.
If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section)
*/


//1
console.log("");

let section = " ";

function getStudentSection(score){
	if (score <= 80) {
		section = [score, "Section Ruby"];
		return section
	} else if (score >= 81 && score <= 120) {
		section = [score, "Section Opal"];
		return section
	} else if (score >= 121 && score <= 160) {
		section = [score, "Section Sapphire"];
		return section
	} else if (score >= 161 && score <= 200) {
		section = [score, "Section Diamond"];
		return section
	} 
}

let student1 = getStudentSection(79);
console.log(`Your score is ${student1[0]}. You will proceed to Grade 10 ${student1[1]}`);

let student2 = getStudentSection(99);
console.log(`Your score is ${student2[0]}. You will proceed to Grade 10 ${student2[1]}`);

let student3 = getStudentSection(150);
console.log(`Your score is ${student3[0]}. You will proceed to Grade 10 ${student3[1]}`);

let student4 = getStudentSection(200);
console.log(`Your score is ${student4[0]}. You will proceed to Grade 10 ${student4[1]}`);


/*2.) 
Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.

Sample Data and output:
Example string: 'Web Development Tutorial'
Expected Output: 'Development'

*/

//2
console.log("");

function findLongestWord(string){
	let stringToArray = string.split(" ");
	// console.log(stringToArray);
	var longestWord = "";

	for(var i = 0; i < stringToArray.length; i++){
   		if(stringToArray[i].length > longestWord.length){
   			longestWord = stringToArray[i]
   		}
   	}
   	console.log(longestWord)		
}

findLongestWord('Web Development Tutorial');





/*3.)
Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'*/


//3
console.log("");

function findNoRepeat(character){
	let charArr = character.split('')

	for(let i = 0; i < charArr.length; i++){
		let singleChar = charArr[i];

		if (charArr.indexOf(singleChar) == i && charArr.indexOf(singleChar, i + 1) == -1) {
	      console.log(singleChar)
    	}
	}

}

findNoRepeat('abacddbec');