//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots

*/

let http = require('http');

let myProducts = [
	{
		"name": "ASUS Vivo Book",
		"description": "A mid-level gaming laptop",
		"price": 44000,
		"stocks": "5"
	},
	{
		"name": "ASUS Vivo Book",
		"description": "A mid-level gaming laptop",
		"price": 44000,
		"stocks": "5"
	},
];


let port = 8000;

let server = http.createServer(function(request, response){
	if (request.url == '/' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Welcome to Ordering System")

	} else if(request.url == '/dashboard' && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Welcome to your User's Dashboard")

	} else if (request.url == '/products' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Here are our available products!")

	} else if(request.url = '/addProduct' && request.method == "POST"){
		let request_product = " ";

		request.on('data', function(data){
			request_product += data;
		});

		request.on('end', function(){
			request_product = JSON.parse(request_product)

			let new_product = {
				"name": request_product.name,
				"description": request_product.description,
				"price": request_product.price,
				"stocks": request_product.stocks
			};

			console.log(new_product);
			myProducts.push(new_product);
			console.log(myProducts);


			response.writeHead(200, {'Content-Type': 'text/plain'})
			response.write(JSON.stringify(new_product))
			response.end("New product added")
		});

	} else if(request.url = '/updateProduct' && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Your product list is updated")

	}  else if(request.url = '/archiveProduct' && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Product successfully archived")
	}
});

server.listen(port);
console.log(`Server is running at localhost: ${port}`);

